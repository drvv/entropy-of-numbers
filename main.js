function number2array(number,base) {
    // find a base expansion and returns an array
    array = number.toString(base);
    return array.split('');

}
function formatArray(array){
    // remove floating point, possibly remove integer part

    index = array.indexOf('.');
    if (index > -1) {
	array.splice(index, 1);
    }
    return array;
}
function getStatistics(array) {
    // takes array which is splitted into chars, count the number of elements
    size = array.length;
    counts = {} // creates empty JSON object
    for (i = 0; i < size; i++){
	counts[array[i]] = counts[array[i]] + 1 || 1; // set 1 for the first appearance
    }
    counts.total = size
    return counts // return JSON object
}
function formatStats(stats,base) {
    // set every possible digit
    for(j = 0; j < base; j++){
	stats[j] = stats[j] || 0;
    }
    return stats;
}
function stats2probdist(stats,base){
    // assumes json object of form {value: count, ... , total: total}
    denom = stats.total;
    for(var key in stats){
	if(Number.isInteger(parseInt(key))){
	    stats[key] = stats[key]/denom;
	} else {
	    delete stats[key];
	}
    }
    // for(i = 0; i < base; i++){
    // 	stats[i] = stats[i]/denom;
    // }
    // delete stats.total;
    return stats;
}
function entropy(probdist, base){
    // calculate entropy for given probability distribution
    entropy_value = 0;
    for(var key in probdist){
	entropy_value += - probdist[key] * baseLog(base, probdist[key]);
    }
    return entropy_value;
}
function baseLog(x, y){
    return Math.log(y) / Math.log(x);
}
