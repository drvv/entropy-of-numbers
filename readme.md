Entropy calculator of real numbers 
==================================

Javascript functions to calculate the entropy of real numbers.

Overview
--------
A real number has its unique base expansion.


This fact could be seen in the following way:  A real number generates a sequence of numbers.  That is any real number could be seen as a message generator.

One of the main properties of generators is entropy.  Thus we define the entropy of real numbers.
